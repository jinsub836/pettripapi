package com.js.pettripapi.controller;

import com.js.pettripapi.entity.MemberInfo;
import com.js.pettripapi.model.member.MemberChangeRequest;
import com.js.pettripapi.model.member.MemberItem;
import com.js.pettripapi.model.member.MemberRequest;
import com.js.pettripapi.model.member.MemberResponse;
import com.js.pettripapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberRequest request){
        memberService.setMember(request);
        return "입력 완료";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers(){return memberService.getMembers();}

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }

    @PutMapping("/change/{id}")
    public String putMember(@PathVariable long id , @RequestBody MemberChangeRequest request){
        memberService.putMember(id, request);
        return "수정이 완료되었습니다.";
    }

    @DeleteMapping("/del/{id}")
    public String delMember(long id){ memberService.delMember(id);
        return "삭제가 완료 되었습니다.";}
}
