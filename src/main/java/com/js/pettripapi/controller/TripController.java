package com.js.pettripapi.controller;

import com.js.pettripapi.entity.MemberInfo;
import com.js.pettripapi.model.trip.TripRequest;
import com.js.pettripapi.service.MemberService;
import com.js.pettripapi.service.TripService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/trip")
public class TripController {
 private final MemberService memberService;
 private final TripService tripService;

 @PostMapping("/new/{memberId}")
 public String setTrip(@PathVariable long memberId, @RequestBody TripRequest request){
  MemberInfo memberInfo = memberService.getData(memberId);
  tripService.setTrip(memberInfo, request);
  return "참석 여부가 등록되었습니다.";
 }
}
