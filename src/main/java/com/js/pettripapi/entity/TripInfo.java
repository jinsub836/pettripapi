package com.js.pettripapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class TripInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId" , nullable = false )
    private MemberInfo memberId;

    @Column(nullable = false)
    private LocalDate writeDate;

    @Column(nullable = false, length = 20)
    private Boolean attendance;

    @Column(nullable = false, length = 20)
    private String tripDate;

    @Column(nullable = false, length = 20)
    private Integer petNumber;
}
