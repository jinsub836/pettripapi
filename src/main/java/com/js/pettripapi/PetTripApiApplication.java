package com.js.pettripapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetTripApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetTripApiApplication.class, args);
    }

}
