package com.js.pettripapi.service;

import com.js.pettripapi.entity.MemberInfo;
import com.js.pettripapi.model.member.MemberChangeRequest;
import com.js.pettripapi.model.member.MemberItem;
import com.js.pettripapi.model.member.MemberRequest;
import com.js.pettripapi.model.member.MemberResponse;
import com.js.pettripapi.resistory.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public MemberInfo getData(long memberId){return memberRepository.findById(memberId).orElseThrow();}
    public void setMember(MemberRequest request){
        MemberInfo addData = new MemberInfo();
        addData.setDateMaker(LocalDate.now());
        addData.setName(request.getName());
        addData.setMobile(request.getMobile());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<MemberInfo> originlist = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for(MemberInfo memberInfo : originlist){
            MemberItem addItem = new MemberItem();
            addItem.setId(memberInfo.getId());
            addItem.setName(memberInfo.getName());

            result.add(addItem);
        } return result;
    }
    public MemberResponse getMember(long id){
        MemberInfo memberInfo = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();

        response.setId(memberInfo.getId());
        response.setName(memberInfo.getName());
        response.setMobile(memberInfo.getMobile());

        return response;
    }

    public void putMember(long id, MemberChangeRequest request){
        MemberInfo memberInfo = memberRepository.findById(id).orElseThrow();
        MemberChangeRequest memberChangeRequest= new MemberChangeRequest();

        memberInfo.setName(memberChangeRequest.getName());
        memberInfo.setMobile(memberChangeRequest.getMobile());

        memberRepository.save(memberInfo);
    }

    public void delMember(long id){memberRepository.deleteById(id);}
}
