package com.js.pettripapi.service;

import com.js.pettripapi.entity.MemberInfo;
import com.js.pettripapi.entity.TripInfo;
import com.js.pettripapi.model.trip.TripRequest;
import com.js.pettripapi.resistory.TripRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class TripService {
    private final TripRepository tripRepository;

    public void setTrip(MemberInfo memberId , TripRequest request){
        TripInfo addData = new TripInfo();
        addData.setWriteDate(LocalDate.now());
        addData.setMemberId(memberId);
        addData.setTripDate(request.getTripDate());
        addData.setAttendance(request.getAttendance());
        addData.setPetNumber(request.getPetNumber());


        tripRepository.save(addData);
    }

}
