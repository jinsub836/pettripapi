package com.js.pettripapi.resistory;

import com.js.pettripapi.entity.TripInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TripRepository extends JpaRepository< TripInfo, Long > {
}
