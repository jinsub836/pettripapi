package com.js.pettripapi.resistory;

import com.js.pettripapi.entity.MemberInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<MemberInfo , Long > {
}
