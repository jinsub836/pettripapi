package com.js.pettripapi.model.trip;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Getter
@Setter
public class TripRequest {
    private Boolean attendance;
    private String tripDate;
    private Integer petNumber;
}
