package com.js.pettripapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MemberResponse {
    private Long id;
    private String name;
    private String mobile;
}
